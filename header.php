<!DOCTYPE html>
<HTML <?php language_attributes(); ?>>
<HEAD>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	global $page, $paged;
	wp_title( '&raquo;', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " &raquo; $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' &raquo; ' . sprintf( 'Page %s', max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php
if ( is_singular() && get_option( 'thread_comments' ) )
	wp_enqueue_script( 'comment-reply' );
wp_head();
?>
</HEAD>

<BODY <?php body_class(); ?>>

<header id="header">
	<div id="header-inner">
		<h1 id="site-title"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<div id="social-links">
			<?php noel_facebook(); noel_twitter(); noel_googleplus(); noel_rss(); ?>
		</div>
		<nav class="site-navigation">
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
		</nav>
		<div class="clear"></div>
	</div>
</header>

<div id="banners-main" style="background-image: url('<?php header_image(); ?>');"></div>

<section id="wrapper">
<section id="wrapper-inner">

<div id="main">