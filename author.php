<?php get_header(); noel_banner_primary(); ?>

<div id="container">
	<div id="content">
		
<?php 
	if ( have_posts() )
		the_post();
?>

<div class="entry-header">
<h2 class="entry-title page-title"><?php printf( __( 'Posts written by %s', 'noel' ), "<span class='vcard'><a class='url fn n' href='" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . "' title='" . esc_attr( get_the_author() ) . "' rel='me'>" . get_the_author() . "</a></span>" ); ?></h2>
</div>

<?php
	rewind_posts();
	get_template_part( 'loop', 'archive' );
?>
        
    </div><!-- #content -->
</div><!-- #container -->

<?php get_footer(); ?>